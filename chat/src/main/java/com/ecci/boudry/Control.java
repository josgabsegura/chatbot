package com.ecci.boudry;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ecci.entity.Conocimiento;

@Stateless
@LocalBean
public class Control {

    @PersistenceContext(name = "myds")
    private EntityManager em;

    @SuppressWarnings("unchecked")
    public List<Conocimiento> buscarPorPatternt() {
        return em.createNamedQuery("Conocimiento.findAll").getResultList();
    }

}

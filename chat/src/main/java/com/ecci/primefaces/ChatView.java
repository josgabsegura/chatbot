package com.ecci.primefaces;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import com.ecci.boudry.Control;
import com.ecci.entity.Conocimiento;
import com.ecci.utils.Frases;
/**
 *
 * @author Gabriel
 */
import com.ecci.utils.FreelingSocketClient;
import com.ecci.utils.RespuestaFreelingDto;

@ManagedBean
@ViewScoped
public class ChatView implements Serializable {

	// private final PushContext pushContext =
	// PushContextFactory.getDefault().getPushContext();
	private final EventBus eventBus = EventBusFactory.getDefault().eventBus();

	@ManagedProperty("#{chatUsers}")
	private ChatUsers users;

	@Inject
	private Control control;

	private String privateMessage;

	private String globalMessage;

	private String username;

	private boolean loggedIn;

	private String privateUser;
	private FreelingSocketClient server;
	private Frases frases = new Frases();

	private final static String CHANNEL = "/{room}/";

	public ChatUsers getUsers() {
		return users;
	}

	public void setUsers(ChatUsers users) {
		this.users = users;
	}

	public String getPrivateUser() {
		return privateUser;
	}

	public void setPrivateUser(String privateUser) {
		this.privateUser = privateUser;
	}

	public String getGlobalMessage() {
		return globalMessage;
	}

	public void setGlobalMessage(String globalMessage) {
		this.globalMessage = globalMessage;
	}

	public String getPrivateMessage() {
		return privateMessage;
	}

	public void setPrivateMessage(String privateMessage) {
		this.privateMessage = privateMessage;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public void sendGlobal() {	
			if (globalMessage != null && !globalMessage.equals("\\q")) {
				eventBus.publish(CHANNEL + "*", username + ": " + globalMessage );
				List<Conocimiento> baseConocimiento =control.buscarPorPatternt();
				eventBus.publish(CHANNEL + "*",
						"Soporte" + ": " + frases.cualFraseFree(globalMessage, control.buscarPorPatternt()));
				
				globalMessage = null;
			}		
	}

	public void sendPrivate() throws IOException {
		eventBus.publish(CHANNEL + privateUser, "[PM] " + username + ": " + privateMessage);
		eventBus.publish(CHANNEL + privateUser, "[PM] " + username + ": " + server.processSegment(privateMessage));
		privateMessage = null;
	}

	public void login() {
		RequestContext requestContext = RequestContext.getCurrentInstance();

		if (users.contains(username)) {
			loggedIn = false;
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Username taken", "Try with another username."));
			requestContext.update("growl");
		} else {
			users.add(username);
			requestContext.execute("PF('subscriber').connect('/" + username + "')");
			loggedIn = true;
		}
	}

	public void disconnect() {
		users.remove(username);
		RequestContext.getCurrentInstance().update("form:users");
		eventBus.publish(CHANNEL + "*", username + " left the channel.");
		loggedIn = false;
		username = null;
	}

	@PostConstruct
	public void init() {
//		try {
//			this.server = new FreelingSocketClient("interno.tektonbpo.com", 1024);
//		} catch (IOException ex) {
//			this.eventBus.publish(CHANNEL + "*", ex.getMessage());
//			System.out.println("Error conectando con freeling " + ex.getMessage());
//		}
	}

}

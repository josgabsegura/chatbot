package com.ecci.primefaces;

import org.primefaces.json.JSONObject;
import org.primefaces.push.Encoder;

public final class MessageEncoder implements Encoder<Message, String> {

    // @Override
    public String encode(Message message) {
        return new JSONObject(message).toString();
    }
}

package com.ecci.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecci.entity.Conocimiento;
import com.ecci.primefaces.ChatView;

import info.debatty.java.stringsimilarity.JaroWinkler;


public class Frases {
	private FreelingSocketClient server;

	

	private List<Conocimiento> baseConocimiento;

	private JaroWinkler j;
	private static double P_AP = 0.7;

	public Frases() {
		try {
			j = new JaroWinkler();
			server = new FreelingSocketClient("interno.tektonbpo.com", 1024);
		} catch (IOException e) {
			Logger.getLogger(ChatView.class.getName()).log(Level.SEVERE, null, e);
		}
	}



	public void compararFrases(String f1, String f2) {
		String[] s1 = f1.split(" ");
		String[] s2 = f2.split(" ");

		String[] ap = new String[s1.length];
		for (int i = 0; i < ap.length; i++) {
			ap[i] = "";
		}

		int i = 0;
		for (String b : s1) {
			for (String c : s2) {
				if (j.similarity(b, c) > P_AP) {
					ap[i] = c;
				}
			}
			i++;
		}

		StringBuffer sb = new StringBuffer();

		for (String b : ap) {
			sb.append(b);
			sb.append(" ");
		}
	}

	public String obtenerFrases(String f1, String f2) {
		String[] s1 = f1.split(" ");
		String[] s2 = f2.split(" ");

		String[] ap = new String[s1.length];
		for (int i = 0; i < ap.length; i++) {
			ap[i] = "";
		}
		int i = 0;
		for (String b : s1) {
			for (String c : s2) {
				if (j.similarity(b, c) > P_AP) {
					ap[i] = c;
				}
			}
			i++;
		}

		StringBuffer sb = new StringBuffer();

		for (String b : ap) {
			sb.append(b);
			sb.append(" ");
		}

		return sb.toString();
	}


	public String cualFraseFree(String input,List<Conocimiento> baseConocimiento) {
		this.baseConocimiento = baseConocimiento;
		List<RespuestaFreelingDto> res = this.normalizarMensaje(input);
		
		StringBuffer sb1 = new StringBuffer();
		res.stream().forEach(k -> {
			sb1.append(k.getPalabraFreeling());
			sb1.append(" ");
		});
		double ranking[] = { 0.0, 0.0 };	
		for (int i = 0; i < baseConocimiento.size(); i++) {
			double r = this.j.similarity(baseConocimiento.get(i).getFrase(), this.obtenerFrases(baseConocimiento.get(i).getFrase(), sb1.toString()));
			if (ranking[0] < r) {
				ranking[0] = r;
				ranking[1] = i;
			}
		}
		if (ranking[0] > P_AP) {
			return  baseConocimiento.get((int) ranking[1]).getRespuesta();
		} else {
			return "No entiendo la pregunta?";
		}
	}

	public List<RespuestaFreelingDto> normalizarMensaje(String mensaje) {
		try {
			List<RespuestaFreelingDto> palabras = new ArrayList<>();
			String a[] = server.processSegment(mensaje.toLowerCase()).split("\n");
			for (int i = 0; i < a.length; i++) {
				String aux[] = a[i].split(" ");
				if (aux.length > 1) {
					palabras.add(new RespuestaFreelingDto(aux[0], aux[1], aux[2], aux[3]));
				}
			}
			return palabras;
		} catch (IOException ex) {
			Logger.getLogger(ChatView.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}

}

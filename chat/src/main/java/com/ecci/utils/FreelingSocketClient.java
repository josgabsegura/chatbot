/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecci.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Esteban
 */
public final class FreelingSocketClient {

    private static final String SERVER_READY_MSG = "FL-SERVER-READY";
    private static final String RESET_STATS_MSG = "RESET_STATS";
    private static final String ENCODING = "UTF8";
    private static final String FLUSH_BUFFER_MSG = "FLUSH_BUFFER";
    private static final Integer BUF_SIZE = 2048;

    private Socket socket;
    private DataInputStream bufferEntrada;
    private DataOutputStream bufferSalida;

    public FreelingSocketClient(String host, Integer port) throws IOException {
        this.socket = new Socket(host, port);
        this.socket.setSoLinger(true, 10);
        this.socket.setKeepAlive(true);
        this.socket.setSoTimeout(10000);

        this.bufferEntrada = new DataInputStream(this.socket.getInputStream());
        this.bufferSalida = new DataOutputStream(this.socket.getOutputStream());

        this.writeMessage(this.bufferSalida, FreelingSocketClient.RESET_STATS_MSG, FreelingSocketClient.ENCODING);

        StringBuffer sb = readMessage(this.bufferEntrada);

        if (sb.toString().compareTo(FreelingSocketClient.SERVER_READY_MSG) != 0) {
            System.out.println("Server ready.");
        }
        this.writeMessage(this.bufferSalida, FreelingSocketClient.FLUSH_BUFFER_MSG, FreelingSocketClient.ENCODING);
        this.readMessage(this.bufferEntrada);

    }

    public void writeMessage(DataOutputStream out, String message, String encoding) throws IOException {
        out.write(message.getBytes(encoding));
        out.write(0);
        out.flush();
    }

    private synchronized StringBuffer readMessage(DataInputStream bufferEntrada) throws IOException {
        byte[] buffer = new byte[BUF_SIZE];
        int bl = 0;
        StringBuffer sb = new StringBuffer();
        do {
            bl = bufferEntrada.read(buffer, 0, FreelingSocketClient.BUF_SIZE);
            if (bl > 0) {
                sb.append(new String(buffer, 0, bl));
            }
        } while (bl > 0 && buffer[bl - 1] != 0);
        return sb;
    }

    public String processSegment(String text) throws IOException {
        this.writeMessage(this.bufferSalida, text, FreelingSocketClient.ENCODING);
        StringBuffer sb = readMessage(this.bufferEntrada);
        this.writeMessage(this.bufferSalida, FreelingSocketClient.FLUSH_BUFFER_MSG, FreelingSocketClient.ENCODING);
        this.readMessage(this.bufferEntrada);
        return sb.toString();
    }

    public void close() throws IOException {
        this.socket.close();
    }

    // <editor-fold defaultstate="collapsed" desc="Getters and Setters">
    /**
     * @return the socket
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * @param socket the socket to set
     */
    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    /**
     * @return the bufferEntrada
     */
    public DataInputStream getBufferEntrada() {
        return bufferEntrada;
    }

    /**
     * @param bufferEntrada the bufferEntrada to set
     */
    public void setBufferEntrada(DataInputStream bufferEntrada) {
        this.bufferEntrada = bufferEntrada;
    }

    /**
     * @return the bufferSalida
     */
    public DataOutputStream getBufferSalida() {
        return bufferSalida;
    }

    /**
     * @param bufferSalida the bufferSalida to set
     */
    public void setBufferSalida(DataOutputStream bufferSalida) {
        this.bufferSalida = bufferSalida;
    }
    // </editor-fold>
}

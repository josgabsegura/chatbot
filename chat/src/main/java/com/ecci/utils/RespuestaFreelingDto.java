package com.ecci.utils;

import java.io.Serializable;

public class RespuestaFreelingDto implements Serializable {

	private String palabraOriginal;
	private String palabraFreeling;
	private String composicion;
	private Double porcentaje;
	
	

	public RespuestaFreelingDto(String palabraOriginal, String palabraFreeling, String composicion, String porcentaje) {
		
		this.palabraOriginal = palabraOriginal;
		this.palabraFreeling = palabraFreeling;
		this.composicion = composicion;
		this.porcentaje = Double.parseDouble(porcentaje);
	}

	public String getPalabraOriginal() {
		return palabraOriginal;
	}

	public void setPalabraOriginal(String palabraOriginal) {
		this.palabraOriginal = palabraOriginal;
	}

	public String getPalabraFreeling() {
		return palabraFreeling;
	}

	public void setPalabraFreeling(String palabraFreeling) {
		this.palabraFreeling = palabraFreeling;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getComposicion() {
		return composicion;
	}

	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}
}
